import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * DateUtil
 */

public class DateUtil {

    private DateUtil(){}

    public static Date createDate(int anyo, int mes, int dia){

        Calendar cal = Calendar.getInstance();
        cal.set(anyo, mes-1, dia);
        return cal.getTime();
    }

    public static Date createDateTime(int anyo, int mes, int dia, int hora, int minutos, int segundos) {
        
        Calendar calTime = Calendar.getInstance();
        calTime.set(anyo, mes-1, dia, hora, minutos, segundos);
        return calTime.getTime();
    }

    public static String formatDate(Date fecha, String plantilla) {
        
        SimpleDateFormat sdf = new SimpleDateFormat(plantilla);
        return sdf.format(fecha);

    }

    public static int cuentaDomingos(int anyo, int mes) {
        
        int domingos=0;
        int mesactual = mes - 1;
        Calendar cal = Calendar.getInstance();
        cal.set(anyo, mesactual, 1);

        while (cal.get(Calendar.MONTH)==mesactual) {
           
            if(cal.get(Calendar.DAY_OF_WEEK)==1){
                domingos++;
                System.out.println(cal.getTime());
            }
            cal.add(Calendar.DATE, 1);
        }

        // System.out.println(cal.get(Calendar.DAY_OF_MONTH));
        // System.out.println(cal.get(Calendar.MONTH));
        // System.out.println(cal.get(Calendar.DAY_OF_WEEK));
        // return 0;

        return domingos;

    }

}