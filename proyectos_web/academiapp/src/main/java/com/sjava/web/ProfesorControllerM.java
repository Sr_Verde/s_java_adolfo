package com.sjava.web;

import java.util.ArrayList;
import java.util.List;

/*
datos en memoria!
*/

public class ProfesorControllerM {
    private static List<Profesor> listaProfesors = new ArrayList<Profesor>();
    private static int contador = 0;

    static {

        // profesor = (idprofesor, nombre, email, telefono, especialidad)
        // Profesor a = new Profesor(3, "ricard", "ricard@gmail.com", 1234, informatica);
        // listaProfesors.add(a);
    }

    // getAll devuelve la lista completa
    public static List<Profesor> getAll(){
        return listaProfesors;
    }

    //getId devuelve un registro
    public static Profesor getId(int id){
        for (Profesor profe : listaProfesors) {
            if (profe.getId()==id){
                return profe;
            }
        }
        return null;
    }
   
    //save guarda un Profesor
    // si es nuevo (id==0) lo añade a la lista
    // si ya existe, actualiza los cambios
    public static void save(Profesor profe) {
        if (profe.getId() == 0){
            contador++;
            profe.setId(contador);
            listaProfesors.add(profe);
        } else {
            for (Profesor profei : listaProfesors) {
                if (profei.getId()==profe.getId()) {
                   
                    profei.setNombre(profe.getNombre());
                    profei.setEmail(profe.getEmail());
                    profei.setTelefono(profe.getTelefono());
                    profei.setEspecialidad(profe.getEspecialidad());
                    break;
                }
            }
        }
        
    }

    // size devuelve numero de Profesors
    public static int size() {
        return listaProfesors.size();
    }


    // removeId elimina Profesor por id
    public static void removeId(int id){
        Profesor borrar=null;
        for (Profesor p : listaProfesors) {
            if (p.getId()==id){
                borrar = p;
                break;
            }
        }
        if (borrar!=null) {
            listaProfesors.remove(borrar);
        }
    }

}