package com.sjava.web;

import java.util.ArrayList;
import java.util.List;

/*
datos en memoria!
*/

public class CursoControllerM {
    private static List<Curso> listaCursos = new ArrayList<Curso>();
    private static int contador = 0;

    static {

        // Curso a = new Curso(idcursos, nombre, horas, profesor_idprofesor);
        // Curso a = new Curso(1,"JQuerv", 15, 0);
        // listaCursos.add(a);
        
    }

    // getAll devuelve la lista completa
    public static List<Curso> getAll(){
        return listaCursos;
    }

    //getId devuelve un registro
    public static Curso getId(int id){
        for (Curso cursoi : listaCursos) {
            if (cursoi.getId()==id){
                return cursoi;
            }
        }
        return null;
    }
   
    //save guarda un Curso
    // si es nuevo (id==0) lo añade a la lista
    // si ya existe, actualiza los cambios
    public static void save(Curso curso) {
        if (curso.getId() == 0){
            contador++;
            curso.setId(contador);
            listaCursos.add(curso);
        } else {
            for (Curso cursoi : listaCursos) {
                if (cursoi.getId()==curso.getId()) {
                   
                    cursoi.setNombre(curso.getNombre());
                    cursoi.setHoras(curso.getHoras());
                    break;
                }
            }
        }
        
    }

    // size devuelve numero de Cursos
    public static int size() {
        return listaCursos.size();
    }


    // removeId elimina Curso por id
    public static void removeId(int id){
        Curso borrar=null;
        for (Curso cursoi : listaCursos) {
            if (cursoi.getId()==id){
                borrar = cursoi;
                break;
            }
        }
        if (borrar!=null) {
            listaCursos.remove(borrar);
        }
    }

}