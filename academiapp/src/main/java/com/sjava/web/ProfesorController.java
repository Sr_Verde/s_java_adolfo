package com.sjava.web;

import java.util.ArrayList;
import java.util.List;

public class ProfesorController {
    private static List<Profesor> listaProfesors = new ArrayList<Profesor>();
    private static int contador = 0;

    static {

        // Profesor a = new Profesor(99,"ricard hernández", "algo@algo.com", "999333");
        // listaProfesors.add(a);
    }

    // getAll devuelve la lista completa
    public static List<Profesor> getAll(){
        return listaProfesors;
    }

    //getId devuelve un registro
    public static Profesor getId(int id){
        for (Profesor p : listaProfesors) {
            if (p.getId()==id){
                return p;
            }
        }
        return null;
    }
   
    //save guarda un Profesor
    // si es nuevo (id==0) lo añade a la lista
    // si ya existe, actualiza los cambios
    public static void save(Profesor pro) {
        if (pro.getId() == 0){
            contador++;
            pro.setId(contador);
            listaProfesors.add(pro);
        } else {
            for (Profesor iproc : listaProfesors) {
                if (iproc.getId()==pro.getId()) {
                   
                    iproc.setNombre(pro.getNombre());
                    iproc.setEmail(pro.getEmail());
                    iproc.setTelefono(pro.getTelefono());
                    iproc.setEspecialidad(pro.getEspecialidad());
               break;
                }
            }
        }
        
    }

    // size devuelve numero de Profesors
    public static int size() {
        return listaProfesors.size();
    }


    // removeId elimina Profesor por id
    public static void removeId(int id){
        Profesor borrar=null;
        for (Profesor p : listaProfesors) {
            if (p.getId()==id){
                borrar = p;
                break;
            }
        }
        if (borrar!=null) {
            listaProfesors.remove(borrar);
        }
    }

}