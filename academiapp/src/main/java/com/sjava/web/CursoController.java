package com.sjava.web;

import java.util.ArrayList;
import java.util.List;

public class CursoController {
    private static List<Curso> listaCurso = new ArrayList<Curso>();
    private static int contador = 0;

    static {

        // Alumno a = new Alumno(99,"ricard hernández", "algo@algo.com", "999333");
        // listaAlumnos.add(a);
    }

    // getAll devuelve la lista completa
    public static List<Curso> getAll(){
        return listaCurso;
    }

    //getId devuelve un registro
    public static Curso getId(int id){
        for (Curso c : listaCurso) {
            if (c.getId()==id){
                return c;
            }
        }
        return null;
    }
   
    //save guarda un alumno
    // si es nuevo (id==0) lo añade a la lista
    // si ya existe, actualiza los cambios
    public static void save(Curso c) {
        if (c.getId() == 0){
            contador++;
            c.setId(contador);
            listaCurso.add(c);
        } else {
            for (Curso cur : listaCurso) {
                if (cur.getId()==c.getId()) {
                   
                    cur.setdescripcion(c.getdescripcion());
                    cur.setnumeroHoras(c.getnumeroHoras());

                    break;
                }
            }
        }
        
    }

    // size devuelve numero de alumnos
    public static int size() {
        return listaCurso.size();
    }


    // removeId elimina alumno por id
    public static void removeId(int id){
        Curso borrar=null;
        for (Curso c : listaCurso) {
            if (c.getId()==id){
                borrar = c;
                break;
            }
        }
        if (borrar!=null) {
            listaCurso.remove(borrar);
        }
    }

}