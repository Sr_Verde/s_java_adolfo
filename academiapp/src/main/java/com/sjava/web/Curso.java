package com.sjava.web;

public class Curso {

    private int id;
    private String descripcion;
    private int numeroHoras;

    public Curso(String descripcion, int numeroHoras){ 
        this.descripcion = descripcion;
        this.numeroHoras = numeroHoras;
        // cambio! no se añade de forma automática, se obliga a ejecutar el método "guarda" de AlumnoController
        // AlumnoController.nuevoContacto(this);
    }

    public Curso(int id, String descripcion, int numeroHoras) {
        this.id = id;
        this.descripcion = descripcion;
        this.numeroHoras = numeroHoras;
    }

    public int getId() {
        return this.id;
    }

    protected void setId(int id){
        this.id=id;
    }


    public String getdescripcion(){
        return this.descripcion;
    }

    public void setdescripcion(String descripcion){
        this.descripcion = descripcion;
    }



    public int getnumeroHoras(){
        return this.numeroHoras;
    }

    public void setnumeroHoras(int numeroHoras){
        this.numeroHoras = numeroHoras;
    }
    
    @Override
    public String toString() {
        return String.format("%s (%s)", this.descripcion, this.numeroHoras);
    }
  
}