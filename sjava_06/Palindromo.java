import java.util.Scanner;

class Palindromo {

    public static void main(String[] args) {
        System.out.println("Introduce una palabra por teclado y la mostrara invertida");
        String palabra;
        String invertida;
        char[] convertido;
        StringBuilder sb = new StringBuilder();

        Scanner keyboard = new Scanner(System.in);
        palabra = keyboard.nextLine();
        convertido = palabra.toCharArray();

        System.out.println(convertido.length);
        System.out.println(convertido[2]);
        for (int i = convertido.length-1; i >= 0; i--) {
            System.out.println(convertido[i]);
            sb.append(convertido[i]);
        }

        invertida = sb.toString();
        String parte1 = invertida.substring(0,1);
        String parte2 = invertida.substring(1);

        
        System.out.println(parte1.toUpperCase() + parte2);

        // keyboard.next();
        keyboard.close();

    }

}