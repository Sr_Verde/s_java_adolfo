package com.sjava;

public class App 
{
    public static void main( String[] args )
    {
        new Persona("ana", 1995, "ana@gmail.com");
        new Persona("claudia", 1999, "claudia@gmail.com");
        new Persona("alfredo", 1998, "alfredo@gmail.com");
        new Persona("alfonso", 2000, "alfonso@gmail.com");
        new Persona("alonso", 2005, "alonso@gmail.com");
        
        System.out.println("muestra lista de contactos: ");
        System.out.println("----------------------");
        PersonaController.muestraContactos();
        System.out.println("----------------------");
        System.out.println("muestra el contacto 2");
        System.out.println("----------------------");
        PersonaController.muestraContactosID(2);
        System.out.println("----------------------");
        System.out.println("borra el contacto 3");
        System.out.println("----------------------");
        PersonaController.borraContactosID(3);
        System.out.println("----------------------");
        System.out.println("muestra lista de contactos actualizada: ");
        System.out.println("----------------------");
        PersonaController.muestraContactos();
        System.out.println("----------------------");
    }
}
