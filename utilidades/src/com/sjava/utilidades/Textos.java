package com.sjava.utilidades;

public class Textos {

    public static String capitaliza(String in) {
        return in.substring(0,1).toUpperCase() + in.substring(1).toLowerCase();
    }
}
