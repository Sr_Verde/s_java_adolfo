
import java.util.Scanner;
import java.util.Random;
import javafx.geometry.Pos;

/**
 * Juego tic-tac-toe
 * 
 * Método principal "play" Curso Java Salamanca Mayo/Junio 2018 Mini-Proyecto
 * 
 * @author XXX
 * @version 1.0
 * 
 */
class Tictactoe {

    // constantes utilizadas en el código
    final String[] SIMBOLOS = new String[] { "-", "X", "O" };
    final int GANA1 = 1;
    final int GANA2 = 2;
    final int EMPATE = 0;
    final int SEGUIR = -1;
    // array 2D (matriz) de posiciones ganadoras
    final int[][] WINNERS = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 }, { 1, 4, 7 }, { 2, 5, 8 }, { 3, 6, 9 }, { 1, 5, 9 },
            { 7, 5, 3 } };

    // mapa de posiciones ocupadas, un array de 9 enteros indicando:
    // (0) posición libre (1) jugador1 (2) jugador2
    int[] map = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    // inicializamos keyboard como atributo de clase para poderlo utilizar en varios
    // métodos
    Scanner keyboard = new Scanner(System.in);

    /**
     * Método principal Crea un bucle indefinido en que ejecuta los dos turnos, uno
     * para cada jugador Siempre empieza el jugador 1 En cada turno se llama al
     * método "turno" que pide la jugada y redibuja el mapa
     * 
     */

    public void play() {

        int respuesta;

        // dibujamos tablero por primera vez
        draw();

        do {
            // invocamos turno con número de jugador y recibimos respuesta
            respuesta = turno(1);
            // si respuesta es distinta de SEGUIR, saltamos el turno 2
            if (respuesta != SEGUIR)
                continue;
            // turno 2, ejecutamos y esperamos respuesta
            respuesta = turno(2);

        } while (respuesta == SEGUIR);

        // hemos salido del bucle, por tanto tenemos o ganador o empate
        // mostramos mensaje adecuado
        switch (respuesta) {
        case GANA1:
            System.out.println("Gana el jugador 1");
            break;
        case GANA2:
            System.out.println("Gana el jugador 2");
            break;
        default:
            System.out.println("Empate, no hay más posiciones");
            break;
        }

        // cerramos teclado, aunque no es imprescindible
        keyboard.close();
    }

    /**
     * método que ejecuta la jugada: pide posición al jugador y devuelve resultado
     * A COMPLETAR: debería verificar si la posición está ocupada, y en este caso vo
     * verla a pedir
     */
    int turno(int jugador) {
        int resp;
        int posicion;
            // VS la máquina. Turno de la maquina:
        if (jugador == 2) {

            int otro = 1;

                posicion = jugada_ganadora(jugador);

                if (posicion==0){
                    posicion = jugada_ganadora(otro);
                }
                
                if (posicion==0){
                    Random random = new Random();
                    do {
                     posicion = random.nextInt(9) + 1;
                    } while (getMap(posicion)>0);
                
                }
                
                setMap(posicion,jugador);

        }
        else {
            // mostramos pregunta y esperamos número introducido
            posicion=0;
            do {
                
                System.out.printf("Jugador %d: ", jugador);
                 try {
                    // establecemos posición en el mapa para jugador actual
                    posicion = keyboard.nextInt();
                }
                catch (Exception e) {
                    System.out.println("valor introducido es incorrecto");
                    keyboard.next();
                    continue;
                }
                if (posicion < 1 || posicion > 9) {
                    System.out.println("la posicion es incorrecta");
                    continue;
                }   
                if (getMap(posicion) > 0) {
                    System.out.println("la posicion ya esta ocupada");
                    continue;
                }
            } while (posicion <= 0 || posicion > 9 || getMap(posicion) > 0);
            
            setMap(posicion, jugador);
        }
    // mostramos mapa actualizado

    draw();

    // calcula respuesta que debe retornar
    if(winner(jugador)) {
            resp = (jugador == 1) ? GANA1 : GANA2;
        } else if (numZeros() == 0) {
            resp = EMPATE;
        } else {
            resp = SEGUIR;
        }
        return resp;
    }

    // VS la máquina. Turno de la maquina:

    /**
     * método que verifica si en las posiciones actuales del tablero el jugador 
     * recibido es el ganador, y en este caso devolver true, de lo contrario false A
     * COMPLETAR: debe verificar realmente las jugadas! pista: utilizar el array
     * WINNERS... montar un bucle que recorra todo el array y compruebe las ternas
     * en las que sean ganadores alguno de los jugadores
     */
    boolean winner(int jugador) {
        for (int[] trio : WINNERS) {
            int valor1 = getMap(trio[0]);
            int valor2 = getMap(trio[1]);
            int valor3 = getMap(trio[2]);

            if (valor1 == 1 && valor2 == 1 && valor3 == 1) {
                System.out.println("El jugador 1 gana la partida");
                return true;
            }

            else if (valor1 == 2 && valor2 == 2 && valor3 == 2) {
                System.out.println("El jugador 2 gana la partida");
                return true;
            }
        }
        return false;
    }

    // devuelve el número de posiciones 0 que hay en el tablero
    // si no queda ninguna, significará que se ha terminado la partida (devuelve 0)
    int numZeros() {
        int zeros = 0;
        for (int i : map) {
            if (i == 0)
                zeros++;
        }
        return zeros;
    }

    // establece la posición del mapa al valor recibido (1/2 segun jugador)
    // IMPORTANTE restamos 1 a la posición puesto que las posiciones van de 1 a 9 y 
    // l array de 0 a 8!
    void setMap(int posicion, int valor) {
        this.map[posicion - 1] = valor;
    }

    // devuelve el valor que hay en la posición recibida. 
    // restamos también una unidad a la posición!
    int getMap(int posicion) {
        return this.map[posicion - 1];
    }

    // muesta mapa en pantalla, MEJORABLE
    // final String[] SIMBOLOS = new String[] {"-","X","O"};
    public void draw() {
        String l1 = this.SIMBOLOS[getMap(1)] + "  " + this.SIMBOLOS[getMap(2)] + "  " + this.SIMBOLOS[getMap(3)];
        String l2 = this.SIMBOLOS[getMap(4)] + "  " + this.SIMBOLOS[getMap(5)] + "  " + this.SIMBOLOS[getMap(6)];
        String l3 = this.SIMBOLOS[getMap(7)] + "  " + this.SIMBOLOS[getMap(8)] + "  " + this.SIMBOLOS[getMap(9)];
        System.out.println();
        System.out.println();
        System.out.println("3enRAYA");
        System.out.println("-------");
        System.out.println(l1);
        System.out.println(l2);
        System.out.println(l3);
        System.out.println("-------");

    }


// Propuesta de inteligencia //

    int jugada_ganadora(int jugador){
    // filas //
    if( getMap(1)==jugador && getMap(2)==jugador && getMap(3)==0) return 3;
    if( getMap(1)==jugador && getMap(3)==jugador && getMap(2)==0) return 2;
    if( getMap(2)==jugador && getMap(3)==jugador && getMap(1)==0) return 1;
    if( getMap(4)==jugador && getMap(5)==jugador && getMap(6)==0) return 6;
    if( getMap(4)==jugador && getMap(6)==jugador && getMap(5)==0) return 5;
    if( getMap(5)==jugador && getMap(6)==jugador && getMap(4)==0) return 4;
    if( getMap(7)==jugador && getMap(8)==jugador && getMap(9)==0) return 9;
    if( getMap(7)==jugador && getMap(9)==jugador && getMap(8)==0) return 8;
    if( getMap(8)==jugador && getMap(9)==jugador && getMap(7)==0) return 7;

    // columnas //
    if( getMap(1)==jugador && getMap(4)==jugador && getMap(7)==0) return 7;
    if( getMap(1)==jugador && getMap(7)==jugador && getMap(4)==0) return 4;
    if( getMap(4)==jugador && getMap(7)==jugador && getMap(1)==0) return 1;
    if( getMap(2)==jugador && getMap(5)==jugador && getMap(8)==0) return 8;
    if( getMap(2)==jugador && getMap(8)==jugador && getMap(5)==0) return 5;
    if( getMap(5)==jugador && getMap(8)==jugador && getMap(2)==0) return 2;
    if( getMap(3)==jugador && getMap(6)==jugador && getMap(9)==0) return 9;
    if( getMap(3)==jugador && getMap(9)==jugador && getMap(6)==0) return 6;
    if( getMap(6)==jugador && getMap(9)==jugador && getMap(3)==0) return 3;

     // diagonales //
    if( getMap(7)==jugador && getMap(5)==jugador && getMap(3)==0) return 3;
    if( getMap(7)==jugador && getMap(3)==jugador && getMap(5)==0) return 5;
    if( getMap(5)==jugador && getMap(3)==jugador && getMap(7)==0) return 7;
    if( getMap(1)==jugador && getMap(5)==jugador && getMap(9)==0) return 9;
    if( getMap(1)==jugador && getMap(9)==jugador && getMap(5)==0) return 5;
    if( getMap(5)==jugador && getMap(9)==jugador && getMap(1)==0) return 1;
     
        return 0;
    }

}