import java.util.Scanner;
import java.util.Arrays;
import java.util.Random;
import javafx.geometry.Pos;


/**
 * Juego Buscaminas
 * 
 * Método principal "play" Curso Java Salamanca Mayo/Junio 2018 Mini-Proyecto
 * 
 * @author Adolfo Fernández Alves
 * @version 1.0
 * 
 */

class Buscaminas{
  

    Scanner keyboard = new Scanner(System.in);

    /* 

    /* Tablero dibujado de buscminas de Buscaminas */
    public void draw() {
        System.out.println("Tablero de buscaminas");
        System.out.println("-----------------------------------------------");


        System.out.println("-----------------------------------------------");
    }

    public void inicio (){
        int dificultad=0;
        System.out.println("Introduzca el nivel de dificultad: facil(1), medio(2), alto(3)");

    do{
        
        try {
                
            dificultad = keyboard.nextInt();

        } catch (Exception e) {
            System.out.println("***Dato incorrecto - introduce 1, 2 o 3");
            keyboard.next();
        }
    }
    while(dificultad !=1 || dificultad !=2 || dificultad != 3);

        switch(dificultad){
            case 1:
            System.out.println("Nivel de dificultad facil");
            break;

            case 2:
            System.out.println("Nivel de dificultad medio");
            break;

            case 3:
            System.out.println("Nivel de dificultad dificil");
            break;

        }


    }

    /* generacion del tablero y de minas */
    public void facil(){

        int [][] tablero = new int[8][8];
        int nbombas = 10;
        int numeritos=0;

        Random random = new Random();

    for (k=1; i<=nbombas; k++){

        bomba_i = random.nextInt(8) + 1;
        bomba_j = random.nextInt(8) + 1;

        for(int j=0; j< tablero.length; j++){
            for(int i=0; i< tablero.length; i++){

                if(bomba_i==tablero[i] && bomba_j==tablero[j]){

                StringBuffer bomba = new StringBuffer();
                tablero[i][j]="O";
                
                }else{
                StringBuffer numeros = new StringBuffer();
                tablero[i][j]="-";
                }

            }
        }

    }


    }

    public void comprobacionfacil(){
        /*Idea básica 
        int numero=0;

        for (i=0; i<tabla.length i++){
            for(j=0; j<tabla.length j++){

                if (tabla[i,j] == "O")
                {
                    for(j-1; j<=j+1; j++){
                        for(i-1; i<=i+1; i++){

                            / 1 BOMBAS /

                            if(table[i][j+1]=="-"
                            && table[i+1][j-1]=="-" && table[i+1][j]=="-" && table[i+1][j+1]=="-"){
                            
                            table[i][j+1]= num++;
                            table[i+1][j-1]= num++;
                            table[i+1][j]= num++;
                            table[i+1][j+1]== num++;}

                            / 2 BOMBAS /

                            if(table[i][j+1]=="O"
                            && table[i+1][j-1]=="-" && table[i+1][j]=="-" && table[i+1][j+1]=="-"){
                            
                            table[i+1][j-1]= num++;
                            table[i+1][j]= num++;
                            table[i+1][j+1]== num++;}
                            
                            if(table[i][j+1]=="-"
                            && table[i+1][j-1]=="O" && table[i+1][j]=="-" && table[i+1][j+1]=="-"){
                            
                            table[i][j+1]= num++;
                            table[i+1][j]= num++;
                            table[i+1][j+1]== num++;}

                            if(table[i][j+1]=="-"
                            && table[i+1][j-1]=="-" && table[i+1][j]=="O" && table[i+1][j+1]=="-"){
                            
                            table[i][j+1]= num++;
                            table[i+1][j-1]= num++;
                            table[i+1][j+1]== num++;}

                            if(table[i][j+1]=="-"
                            && table[i+1][j-1]=="-" && table[i+1][j]=="-" && table[i+1][j+1]=="O"){
                            
                            table[i][j+1]= num++;
                            table[i+1][j-1]= num++;
                            table[i+1][j]= num++;}

                            / 3 BOMBAS /

                            if(table[i][j+1]=="O"
                            && table[i+1][j-1]=="O" && table[i+1][j]=="-" && table[i+1][j+1]=="-"){
                            
                            table[i+1][j]= num++;
                            table[i+1][j+1]== num++;}

                            if(table[i][j+1]=="O"
                            && table[i+1][j-1]=="-" && table[i+1][j]=="O" && table[i+1][j+1]=="-"){
                            
                            table[i+1][j-1]= num++;
                            table[i+1][j+1]== num++;}
                            
                            if(table[i][j+1]=="O"
                            && table[i+1][j-1]=="-" && table[i+1][j]=="-" && table[i+1][j+1]=="O"){
                            
                            table[i+1][j-1]= num++;
                            table[i+1][j]= num++;}
                            
                            if(table[i][j+1]=="-"
                            && table[i+1][j-1]=="O" && table[i+1][j]=="O" && table[i+1][j+1]=="-"){
                            
                            table[i][j+1]= num++;
                            table[i+1][j+1]== num++;}                          
                            
                            if(table[i][j+1]=="-"
                            && table[i+1][j-1]=="O" && table[i+1][j]=="-" && table[i+1][j+1]=="O"){
                            
                            table[i][j+1]= num++;
                            table[i+1][j]= num++;}
                            
                            if(table[i][j+1]=="-"
                            && table[i+1][j-1]=="-" && table[i+1][j]=="O" && table[i+1][j+1]=="O"){
                            
                            table[i+1][j]= num++;
                            table[i+1][j+1]== num++;}
                            
                            / 4 BOMBAS /

                            if(table[i][j+1]=="O"
                            && table[i+1][j-1]=="O" && table[i+1][j]=="O" && table[i+1][j+1]=="-"){
                            
                            table[i+1][j+1]== num++;}
                            
                            if(table[i][j+1]=="O"
                            && table[i+1][j-1]=="O" && table[i+1][j]=="-" && table[i+1][j+1]=="O"){
                            

                            table[i+1][j]= num++;}

                            if(table[i][j+1]=="O"
                            && table[i+1][j-1]=="-" && table[i+1][j]=="O" && table[i+1][j+1]=="O"){
                            
                            table[i+1][j-1]= num++;}
                            
                            if(table[i][j+1]=="-"
                            && table[i+1][j-1]=="O" && table[i+1][j]=="O" && table[i+1][j+1]=="O"){
                            
                            table[i][j+1]= num++;}
                            
                            /5 BOMBAS/

                            if(table[i][j+1]=="O"
                            && table[i+1][j-1]=="O" && table[i+1][j]=="O" && table[i+1][j+1]=="O"){}
                            
                          


                        }
                    }
                }

                if (tabla[i,j] == "O" && tabla[i+1,j+1]=="O"
                    
                {
                        for(j-1; j<=j+1; j++){
                            for(i-1; i<=i+1; i++){
                                table[i][j]=num+;
                            }
                        }
                }
            }
        }
        
    }


        */
    }

    //-> Clase de lista de bombas, y guardarlas en un array!

    

}

