class Animalitos{

    public static void main(String[] args) {
    /* Teclear el código para que se creen instancias de cada una de las clases
    y ejecute sus métodos comunes y los métodos específicos definidos en cada tipo de animal.*/

    Animal a = new Perro();
    Animal b = new Pajaro();
    Animal c = new Serpiente();

    System.out.print("El perro hace: ");
    ((Perro) a).ladra();
    System.out.print(" y tiene: "+ a.getNumeroPatas() + " patas");

    System.out.println("");

    System.out.print("La serpiente: ");
    ((Serpiente) c).repta();

    System.out.println("");

    System.out.print("El pajaro: ");
    ((Pajaro) b).vuela();
    System.out.print(" y tiene: "+ b.getNumeroPatas() + " patas");

    }
}