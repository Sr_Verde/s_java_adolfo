package com.sjava.testmoto;

class Moto {
    String marca;
    String modelo;
    int cc;
    int cv;
    int pvp;
    int precio;

    public Moto(String marca, String modelo, int cc, int cv, int pvp, int precio) {
        this.marca = marca;
        this.modelo = modelo;
        this.cc = cc;
        this.cv = cv;
        this.pvp = pvp;
        this.precio = precio;
    }

    public void comparaCv(Moto otra){
        if (this.cv > otra.cv) {
            System.out.println(
                "La %s %s es más potente que la %s %s",
                this.marca, this.modelo, otra.marca, otra.modelo);
        } else {
            System.out.println(
                "La %s %s es más potente que la %s %s",
                otra.marca, otra.modelo, this.marca, this.modelo);   
        }
    }

    public void comparaPrecio(Moto otra){
        if (this.precio > otra.precio){
            System.out.println(
                "El precio de %s %s es %d que es mayor que el precio de %s %s que es %d",
                this.marca, this.modelo, this.precio, otra.marca, otra.modelo, otra.precio);
        } else {
            System.out.println(
                "El precio de %s %s es %d que es menor que el precio de %s %s que es %d",
                this.marca, this.modelo, this.precio, otra.marca, otra.modelo, otra.precio);
        }
    }
}
