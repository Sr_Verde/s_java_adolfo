import java.util.Scanner;

class InfoNumsK {

    public static void main(String[] args) {

        Scanner keyboard = new Scanner(System.in);
        float total = 0;
        int num = 0;
        int contador = 0;
        int maximo = 0;
        int minimo = 0;
        do {
            System.out.printf("Entra un num: ");
            try {

                num = keyboard.nextInt();

                if (contador == 0) {
                    maximo = num;
                    minimo = num;
                    contador++;
                    total += num;
                }

            } catch (Exception e) {
                System.out.println("***Dato incorrecto - 0 para salir***");
                keyboard.next();
            }

            if (num != 0) {
                maximo = (num > maximo) ? num : maximo;
                minimo = (num < minimo) ? num : minimo;
                total += num;
                contador++;
            }

        } while (num > 0);
        System.out.println("La suma es " + total);
        System.out.println("La media es " + total/contador);
        System.out.println("Los numeros introducidos es " + contador);
        System.err.println("El maximo es " + maximo);
        System.out.println("El minimo es " + minimo);
        keyboard.close();
    }
}