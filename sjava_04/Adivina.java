import java.util.Scanner;
import java.util.Random;

// //package a importar al principio del archivo de clase 
// import java.util.Random; 
//  
// //código que genera un aleatorio de 1 a 10 
// Random random = new Random(); 
// int incognita = random.nextInt(10)+1; 

class Adivina {

    public static void main(String[] args) {
        Random random = new Random();
        int incognita = random.nextInt(10) + 1;

        System.out.println("Adivina un numero del 1 al 10");

        Scanner keyboard = new Scanner(System.in);
        int contador = 0;
        int num = 0;

        do {
            System.out.printf("Introduce un num: ");
            try {
                
                num = keyboard.nextInt();

            } catch (Exception e) {
                System.out.println("***Dato incorrecto - introduce un número < 1 o > 10 para salir***");
                contador++;
                keyboard.next();
            }

            contador++;

        } while (num > 0 && num < 11 && num != incognita);

        System.out.println("Has acertado! el numero era " + incognita);
        System.out.println("El numero de intentos es: " + contador);
        keyboard.close();
    }

}