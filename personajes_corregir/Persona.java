/**
 *Clase Persona:

Atributos "protected": nombre, edad

Métodos get y set para cada atributo, públicos (getNombre, setNombre, getEdad, setEdad)

Método presentacion(), devuelve un String con la frase de presentación. El string se construye utilizando los métodos getNombre y getEdad.

 */
public class Persona {
    protected String nombre;
    protected int edad;

    public String getNombre(){
        return nombre;
    }

    public void setNombre(String nombre){
        this.nombre = nombre;
    }

    public int getEdad(){
        return edad;
        
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }
    
}