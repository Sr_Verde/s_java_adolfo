<%@page contentType="text/html; charset=UTF-8" %>
<%-- <%@page import="com.sjava.web.Persona" %> --%>

<%
    String titulo = "Mi primer JSP";
    String[] lista = new String[] {"primero", "segundo", "tercero"};
    
%>
<%-- DIOS QUÉ BUENO ESTOY, ME DERRITO AR SOLETE--%>
<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta charset="utf-8">
    <title>Ejercicios HMTL/CSS</title>
    <link rel="stylesheet" type="text/css" href="css/estilos.css">
</head>
<body>

    <h1><% out.print(titulo); %></h1>
    <h1><%= titulo %></h1>
    <br>

    <ul>
        <% for (String s : lista){ %>
        
        <li><%= s %></li>

        <% } %>
    </ul>

</body>
</html>

