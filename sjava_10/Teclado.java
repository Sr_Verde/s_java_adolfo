import java.util.Scanner;
import java.text.Normalizer;
/**
 * teclado
 */
public class Teclado {
    public static int pideNumero(int min, int max) {
        int numero;
        Scanner keyboard = new Scanner(System.in);
        do{
            System.out.println("Introduce un numero: ");
            numero = keyboard.nextInt();
        }while(numero < min || numero > max);
        return numero;
    }

    public static String pideTexto(){
        String texto;
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Introduce texto: ");
        texto = keyboard.nextLine();
        System.out.println("Texto devuelto: ");
        return texto;
    }

    
}

