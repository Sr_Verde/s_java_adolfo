import java.util.Scanner;


public class Test {

    public static void main(String[] args) {
        String frase;
        String resultado;
        String texto;
        int resultadoNum;
        int max;
        int min;
        
        Scanner keyboard = new Scanner(System.in);

        System.out.println("introduzca una palabra: ");
        frase = keyboard.nextLine();
        resultado = Textos.slug(frase);
        
        System.out.println(resultado);

        System.out.println("introduzca un maximo: ");
        max = keyboard.nextInt();
        System.out.println("introduzca un minimo: ");
        min = keyboard.nextInt();

        resultadoNum = Teclado.pideNumero(min, max);
        System.out.println(resultadoNum);

        texto = Teclado.pideTexto();
        System.out.println(texto);

    }
}