// Ejemplo de clase //

class Person {
    // ATRIBUTOS //
    String nombre;
    int edad;

    // CONSTRUCTOR 1º //
    // Person(String n, int e){
    //     nombre = n;
    //     edad = e;
    // }

    public Person(){}
    
    // CONSTRUCTOR 2º //
    Person(String nombre, int edad){
        this.nombre = nombre;
        this.edad = edad;
    }

    // METODOS //
    // void presentacion(){ 
    // System.out.println("Hola me llamo " + nombre);
    // System.out.println("Edad: " + edad);
    
    void presentacion(){ 
        System.out.println("Hola me llamo " + this.nombre);
        System.out.println("Edad: " + this.edad);
    }
}
