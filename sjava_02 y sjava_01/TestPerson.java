class TestPerson {
    public static void main(String[] args) {
        Person p = new Person();
        p.nombre = "Ana";
        p.edad = 35;
        dobleEdad(p);

        System.out.println("Nombre: " + p.nombre);
        System.out.println("Edad: " + p.edad);
    }

    public static void dobleEdad(Person persona){
        persona.edad = persona.edad*2;
    }
}

